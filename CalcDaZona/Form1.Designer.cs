﻿namespace CalcDaZona
{
    partial class calcZona
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(calcZona));
            this.label1 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.vA = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.vB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.vC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbData = new System.Windows.Forms.Label();
            this.dateTimeP = new System.Windows.Forms.DateTimePicker();
            this.grbCalc = new System.Windows.Forms.GroupBox();
            this.rdForma = new System.Windows.Forms.RadioButton();
            this.rdMedia = new System.Windows.Forms.RadioButton();
            this.rdMulti = new System.Windows.Forms.RadioButton();
            this.rdSubst = new System.Windows.Forms.RadioButton();
            this.rdSoma = new System.Windows.Forms.RadioButton();
            this.listView1 = new System.Windows.Forms.ListView();
            this.ZONA = new System.Windows.Forms.Label();
            this.grbCalc.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(53, 9);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(100, 20);
            this.txtNome.TabIndex = 5;
            // 
            // vA
            // 
            this.vA.Location = new System.Drawing.Point(53, 35);
            this.vA.Name = "vA";
            this.vA.Size = new System.Drawing.Size(100, 20);
            this.vA.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Valor A";
            // 
            // vB
            // 
            this.vB.Location = new System.Drawing.Point(53, 61);
            this.vB.Name = "vB";
            this.vB.Size = new System.Drawing.Size(100, 20);
            this.vB.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Valor B";
            // 
            // vC
            // 
            this.vC.Location = new System.Drawing.Point(53, 87);
            this.vC.Name = "vC";
            this.vC.Size = new System.Drawing.Size(100, 20);
            this.vC.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Valor C";
            // 
            // lbData
            // 
            this.lbData.AutoSize = true;
            this.lbData.Location = new System.Drawing.Point(159, 9);
            this.lbData.Name = "lbData";
            this.lbData.Size = new System.Drawing.Size(30, 13);
            this.lbData.TabIndex = 12;
            this.lbData.Text = "Data";
            // 
            // dateTimeP
            // 
            this.dateTimeP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimeP.Location = new System.Drawing.Point(201, 9);
            this.dateTimeP.Name = "dateTimeP";
            this.dateTimeP.Size = new System.Drawing.Size(94, 20);
            this.dateTimeP.TabIndex = 13;
            // 
            // grbCalc
            // 
            this.grbCalc.Controls.Add(this.rdForma);
            this.grbCalc.Controls.Add(this.rdMedia);
            this.grbCalc.Controls.Add(this.rdMulti);
            this.grbCalc.Controls.Add(this.rdSubst);
            this.grbCalc.Controls.Add(this.rdSoma);
            this.grbCalc.Location = new System.Drawing.Point(12, 113);
            this.grbCalc.Name = "grbCalc";
            this.grbCalc.Size = new System.Drawing.Size(200, 100);
            this.grbCalc.TabIndex = 14;
            this.grbCalc.TabStop = false;
            this.grbCalc.Text = "Cqalca";
            // 
            // rdForma
            // 
            this.rdForma.AutoSize = true;
            this.rdForma.Location = new System.Drawing.Point(7, 66);
            this.rdForma.Name = "rdForma";
            this.rdForma.Size = new System.Drawing.Size(119, 17);
            this.rdForma.TabIndex = 4;
            this.rdForma.TabStop = true;
            this.rdForma.Text = "Formula Resolvente";
            this.rdForma.UseVisualStyleBackColor = true;
            // 
            // rdMedia
            // 
            this.rdMedia.AutoSize = true;
            this.rdMedia.Location = new System.Drawing.Point(97, 43);
            this.rdMedia.Name = "rdMedia";
            this.rdMedia.Size = new System.Drawing.Size(54, 17);
            this.rdMedia.TabIndex = 3;
            this.rdMedia.TabStop = true;
            this.rdMedia.Text = "Media";
            this.rdMedia.UseVisualStyleBackColor = true;
            // 
            // rdMulti
            // 
            this.rdMulti.AutoSize = true;
            this.rdMulti.Location = new System.Drawing.Point(7, 43);
            this.rdMulti.Name = "rdMulti";
            this.rdMulti.Size = new System.Drawing.Size(87, 17);
            this.rdMulti.TabIndex = 2;
            this.rdMulti.TabStop = true;
            this.rdMulti.Text = "Multiplicacao";
            this.rdMulti.UseVisualStyleBackColor = true;
            // 
            // rdSubst
            // 
            this.rdSubst.AutoSize = true;
            this.rdSubst.Location = new System.Drawing.Point(98, 20);
            this.rdSubst.Name = "rdSubst";
            this.rdSubst.Size = new System.Drawing.Size(74, 17);
            this.rdSubst.TabIndex = 1;
            this.rdSubst.TabStop = true;
            this.rdSubst.Text = "Subtracao";
            this.rdSubst.UseVisualStyleBackColor = true;
            // 
            // rdSoma
            // 
            this.rdSoma.AutoSize = true;
            this.rdSoma.Location = new System.Drawing.Point(7, 20);
            this.rdSoma.Name = "rdSoma";
            this.rdSoma.Size = new System.Drawing.Size(52, 17);
            this.rdSoma.TabIndex = 0;
            this.rdSoma.TabStop = true;
            this.rdSoma.Text = "Soma";
            this.rdSoma.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(302, 9);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(321, 204);
            this.listView1.TabIndex = 16;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // ZONA
            // 
            this.ZONA.AutoSize = true;
            this.ZONA.Font = new System.Drawing.Font("Comic Sans MS", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZONA.Location = new System.Drawing.Point(143, 216);
            this.ZONA.Name = "ZONA";
            this.ZONA.Size = new System.Drawing.Size(349, 135);
            this.ZONA.TabIndex = 17;
            this.ZONA.Text = "ZONA";
            this.ZONA.Click += new System.EventHandler(this.ZONA_Click);
            // 
            // calcZona
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 355);
            this.Controls.Add(this.ZONA);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.grbCalc);
            this.Controls.Add(this.dateTimeP);
            this.Controls.Add(this.lbData);
            this.Controls.Add(this.vC);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.vB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.vA);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "calcZona";
            this.Text = "Calculo da Zona";
            this.Load += new System.EventHandler(this.calcZona_Load);
            this.grbCalc.ResumeLayout(false);
            this.grbCalc.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox vA;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox vB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox vC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbData;
        private System.Windows.Forms.DateTimePicker dateTimeP;
        private System.Windows.Forms.GroupBox grbCalc;
        private System.Windows.Forms.RadioButton rdForma;
        private System.Windows.Forms.RadioButton rdMedia;
        private System.Windows.Forms.RadioButton rdMulti;
        private System.Windows.Forms.RadioButton rdSubst;
        private System.Windows.Forms.RadioButton rdSoma;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label ZONA;
    }
}

