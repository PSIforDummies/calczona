﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcDaZona
{
    public class ZonaCalculator
    {
        public ZonaCalculator(Double valA, Double valB, Double valC)
        {
            this.valA = valA;
            this.valB = valB;
            this.valC = valC;
        }

        public Double valA { get; set; }
        public Double valB { get; set; }
        public Double valC { get; set; }

        public Double Sum()
        {
            Double Total = valA + valB + valC;
            return Total;

        }

        public Double Sub()
        {
            Double Total = valA - valB - valC;
            return Total;
        }

        public Double Mult()
        {
            Double Total = valA * valB * valC;
            return Total;
        }

        public Double Med()
        {
            Double Total = (valA + valB + valC) / 3;
            return Total;
        }
    }
}
