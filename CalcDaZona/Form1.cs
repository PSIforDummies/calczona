﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalcDaZona
{
    public partial class calcZona : Form
    {
        public calcZona()
        {
            InitializeComponent();
        }

        private void ZONA_Click(object sender, EventArgs e)
        {
            ZonaCalculator calc = new ZonaCalculator(Convert.ToInt32(vA.Text), Convert.ToInt32(vB.Text), Convert.ToInt32(vC.Text));
            if (rdSoma.Checked == true)
            {
                listView1.Items.Add(txtNome.Text + " - " + dateTimeP.Value.Date + " | " + calc.valA + " + " + calc.valB + " + " + calc.valC + " = " + calc.Sum().ToString());
            }
            if (rdSubst.Checked == true)
            {
                listView1.Items.Add(txtNome.Text + " - " + dateTimeP.Value.Date + " | " + calc.valA + " - " + calc.valB + " . " + calc.valC + " = " + calc.Sub().ToString());
            }
            if (rdMulti.Checked == true)
            {
                listView1.Items.Add(txtNome.Text + " - " + dateTimeP.Value.Date + " | " + calc.valA + " x " + calc.valB + " x " + calc.valC + " = " + calc.Mult().ToString());
            }
            if (rdMedia.Checked == true)
            {
                listView1.Items.Add(txtNome.Text + " - " + dateTimeP.Value.Date + " | " + calc.valA + " + " + calc.valB + " + " + calc.valC + " / 3 = " + calc.Med().ToString());
            }
        }

        private void calcZona_Load(object sender, EventArgs e)
        {
            listView1.View = View.List;
        }
    }
}
